package com.wisely.spring_boot_starter_hello;

/**
 * Description:
 * Created by Guo_guo on 2017-8-6.
 */
public class HelloService {
    private String msg;

    public String sayHello () {
        return "hello" + msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
